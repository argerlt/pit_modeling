# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 10:05:39 2021

@author: arger
Just a file for holding extra functions. Makes the other scripts more compact

"""
import glob,os,time,h5py,scipy,re
import numpy as np
import skimage.io as skio
from skimage import measure
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from transformations import rotation_matrix
from stl import mesh





def HDF5_from_slices(read_folder, write_file):
    """Loads up the tif files from a folder of sequentially named cuts and
    builds a data_stack object, which is just a 3D numpy arrary of stacked
    images. Then it saves that 3D array as an HDF5 file containing only a
    single 3D array"""
    # Make a list of the images and sort it, then stack them together
    filelist = glob.glob(read_folder + "/*.tif")  #
    filelist = np.sort(filelist)
    data_stack_u16 = np.stack(
        [skio.imread(file) for file in filelist])  
    # Turn that stack into a uint8 file (this isn't necessary, but will make
    # Everything else run faster since uint8 takes only half the disk space)
    data_stack = (data_stack_u16 / 305).astype(np.uint8)
    # Trim off some of the wasted space before saving, then add a border of zeros
    data_stack = data_stack[49:-3, :-3, 35:1736]
    x, y, z = data_stack.shape
    padded = np.zeros([x + 2, y + 2, z + 2])
    padded[1:-1, 1:-1, 1:-1] = data_stack
    data_stack = padded * 1
    del padded
    # Save the file as an hdf before moving on
    hdf_file = h5py.File(write_file + ".h5", "w")
    hdf_file["Grain_ID"] = data_stack.astype(np.uint8)
    hdf_file.close()
    return data_stack




def get_init_dict():
    """This function returns a dictionary for translating the Tiff Integer 
values into Pore IDs
Quick reference for what the points in the above array mean: 
value My_ID  Original_Name      value My_ID  Original_Name
0    (N/A)   Air                 100  Pore_5  Larger Purple Pit at 8s
20   Pore_1  Blue Pit at 1s      120  Pore_6  Smaller Purple pit at 8s
40   Pore_2  Purple Pit at 1s    140  Pore_7  Purple Pit at 14s
60   Pore_3  Red Pit at 1s       160  Pore_8  Blue Pit at 22s
80   Pore_4  Green Pit at 8s     180  Bulk    Bulk Metal
"""
    return {
        0: "Air",
        20: "Pore_1",
        40: "Pore_2",
        60: "Pore_3",
        80: "Pore_4",
        100: "Pore_5",
        120: "Pore_6",
        140: "Pore_7",
        160: "Pore_8",
        180: "Bulk",
    }


def Load_Initial_Microstructure(file):
    """Quick reference for what Material each integer refers to in this array:
    0      Air                  100    Larger Purple Pit at 8s
    20     Blue Pit at 1s       120    Smaller Purple pit at 8s
    40     Purple Pit at 1s     140    Purple Pit at 14s
    60     Red Pit at 1s        160    Blue Pit at 22s
    80     Green Pit at 8s      180    Bulk Metal"""
    hdf_file = h5py.File(file, "r")
    data_stack = np.asanyarray(hdf_file["Grain_ID"])
    hdf_file.close()
    return data_stack


def Cut_Out_Pore(x_start,y_start,z_start,xlen,ylen,zlen,
                 initial,Pore_ID = 'infer',stl_plot = True, slice_plot = True,
                 reorient = True, ypad = 10, xrot=0.01,zrot=-0.06):
    # Do some quick asserts to make sure the cut we make makes sense
    assert all(isinstance(i,int) for i in [x_start,y_start,z_start]),"""
starting point error: start values MUST be integers, else slicing will fail"""
    assert all(isinstance(i,int) for i in [xlen,ylen,zlen]),"""
slice size error: lengh values MUST be integers, else slicing will fail"""
    assert type(initial) == np.ndarray,"""
initial Error: variable 'initial' should be a numpy array """
    assert len(initial.shape) == 3,"""
initial Error: variable 'initial' should be a three dimensional numpy array """
    assert any([Pore_ID == 'infer', type(Pore_ID) == int]),"""
Pore_ID error: Pore ID must be an integer between 0 and 180, or left undefined"""
    
    ylen =ylen + ypad
    sim_volume = initial[x_start:x_start+xlen,
                         y_start:y_start+ylen,
                         z_start:z_start+zlen]
    l_max = np.max([xlen,ylen,zlen])
    
    if stl_plot == True:
        stl_figname = '{}_pore_shape'.format(Pore_ID)
        plt.close(stl_figname)
        stl_fig =plt.figure(stl_figname)
        stl_ax = stl_fig.add_subplot(111,projection ='3d')
        p_v,p_f = measure.marching_cubes(sim_volume,level =179,step_size = 4)[:2]
        mesh = Poly3DCollection(p_v[p_f])
        mesh.set_edgecolor('k')
        stl_ax.add_collection3d(mesh)
        xx,yy,zz = np.stack(np.meshgrid([0,xlen],[0,ylen-ypad],[0,zlen]))
        stl_ax.voxels(xx,yy,zz,np.array([True]).reshape(1,1,1),facecolors = 'r', edgecolors = 'k',alpha = 0.2)
        stl_ax.set_xlabel('X-axis')
        stl_ax.set_ylabel('Y-axis')
        stl_ax.set_zlabel('Z-axis')
        stl_ax.set_xlim(-1,l_max+1)
        stl_ax.set_ylim(-1,l_max+1)
        stl_ax.set_zlim(-1,l_max+1)
        stl_ax.view_init(elev=113,azim = -81)
    else:
        stl_fig = 'N/A'
        stl_ax = 'N/A'
    
    if slice_plot == True:
        slice_figname = '{}_sliced'.format(Pore_ID)
        s_fig,s_ax = plt.subplots(4,4,num = slice_figname, clear = True)
        for i in np.arange(16):
            if i <(sim_volume.shape[1]--1):
                s_ax[i%4,i//4].imshow(sim_volume[:,ylen-1-i,:],vmax = 180,vmin = 0)
                s_ax[i%4,i//4].text(0,100,ylen-1-i)
            else:
                s_ax[i%4,i//4].imshow(sim_volume[:,0,:]*0,vmax = 180,vmin = 0)

    else:
        s_fig = 'N/A'
        s_ax = 'N/A'
    
    if reorient == True:
    #this is code from here: http://www.lfd.uci.edu/~gohlke/code/transformations.py.html

        coords=np.meshgrid(np.arange(xlen),
                           np.arange(ylen),
                           np.arange(zlen))
        xyz=np.vstack([coords[0].reshape(-1)-float(xlen)/2,     # x coordinate, centered
                       coords[1].reshape(-1)-float(ylen)/2,     # y coordinate, centered
                       coords[2].reshape(-1)-float(zlen)/2,     # z coordinate, centered
                       np.ones((xlen,ylen,zlen)).reshape(-1)])    # 1 for homogeneous coordinates
#        matz=rotation_matrix(-0.06*np.pi,(0,0,1))
#        matx=rotation_matrix(0.01*np.pi,(1,0,0))
        matz=rotation_matrix(zrot*np.pi,(0,0,1))
        matx=rotation_matrix(xrot*np.pi,(1,0,0))
        transformed_xyz=np.dot(matx,np.dot(matz, xyz))

        x=transformed_xyz[0,:]+float(xlen)/2
        y=transformed_xyz[1,:]+float(ylen)/2
        z=transformed_xyz[2,:]+float(zlen)/2

        x=x.reshape((xlen,ylen,zlen))
        y=y.reshape((xlen,ylen,zlen))
        z=z.reshape((xlen,ylen,zlen))
        
        new_xyz=[x,y,z]
        new_vol=scipy.ndimage.map_coordinates(sim_volume,new_xyz, order=0)
        
        final_volume = new_vol.reshape(ylen,xlen,zlen)
        final_volume = np.transpose(final_volume,(1,0,2))
        final_volume =final_volume[:,0:ylen-ypad,:]        
        del new_vol,x,y,z,transformed_xyz,coords,xyz
    else: 
        final_volume =sim_volume[:,0:ylen-ypad,:]*1
    ylen = ylen-ypad
    del sim_volume
    
    if slice_plot == True:
        slice_figname2 = '{}_reoriented'.format(Pore_ID)
        s2_fig,s2_ax = plt.subplots(4,4,num = slice_figname2, clear = True)
        for i in np.arange(16):
            if i <(final_volume.shape[1]-1):
                s2_ax[i%4,i//4].imshow(final_volume[:,ylen-1-i,:],vmax = 180,vmin = 0)
                s2_ax[i%4,i//4].text(0,100,ylen-1-i)
            else:
                s_ax[i%4,i//4].imshow(final_volume[:,0,:]*0,vmax = 180,vmin = 0)
    else:
        s2_fig = 'N/A'
        s2_ax = 'N/A'

    return(final_volume,[stl_fig,stl_ax,s_fig,s_ax,s2_fig,s2_ax])

def Surface_Cleaner(vol_in,Pore_ID,iterations =300, plot = False):
    vol =np.copy(vol_in)
    xlen,ylen,zlen = vol.shape

    if plot == True:
        fig1,ax1 = plt.subplots(4,4,num = 'Pore_{}_Before'.format(Pore_ID), clear = True)
        for i in np.arange(16):
            ax1[i%4,i//4].imshow(vol[:,ylen-1-i,:],vmax = 255,vmin = 50)
            ax1[i%4,i//4].text(0,100,ylen-1-i)

    vol[vol ==180] = 255
    vol[vol ==Pore_ID] = 200
    vol[vol <200] = 0
    
    if plot == True:
        fig1,ax1 = plt.subplots(4,4,num = 'Pore_{}_Beginning'.format(Pore_ID), clear = True)
        for i in np.arange(16):
            ax1[i%4,i//4].imshow(vol[:,ylen-1-i,:],vmax = 255,vmin = 50)
            ax1[i%4,i//4].text(0,100,ylen-1-i)
    
    # Median Filter for 1 iteration    
    noise =scipy.random.randint(1,199,np.prod(vol.shape)).reshape(vol.shape)
    noisy_vol = np.max(np.stack([noise,vol],axis = 3),axis=3)
    mask = noisy_vol<200
    median = scipy.ndimage.median_filter(noisy_vol,2)
    noisy_vol[mask] = median[mask]
    
    #Median filter as necessary for n additional iterations
    iii =1
    while np.min(noisy_vol)<199 and iii<iterations:
        iii = iii+1
        if iii%20 == 0:
            print('median filter iteration {}'.format(iii))
        noisy_vol[noisy_vol<199] = 0
        noise =scipy.random.randint(1,199,np.prod(vol.shape)).reshape(vol.shape)
        noisy_vol = np.max(np.stack([noise,noisy_vol],axis = 3),axis=3)
        mask = noisy_vol<200
        median = scipy.ndimage.median_filter(noisy_vol,(iii%5)+2)
        noisy_vol[mask] = median[mask]
    if plot == True:
        fig,ax = plt.subplots(4,4,num = 'Pore_{}_After'.format(Pore_ID), clear = True)
        for i in np.arange(16):
            ax[i%4,i//4].imshow(noisy_vol[:,ylen-1-i,:],vmax = 255,vmin = 50)
            ax[i%4,i//4].text(0,100,ylen-1-i)
    return(noisy_vol)
        


def input_text_gen(x,y,z,init_name):
    date = "_".join(np.array([i for i in time.localtime()]).astype(str))
    return( """
# ========================================================================== #
# Dummy Script Auto-generated on {}
# ========================================================================== #
#	Materials types, properties, phases
# -------------------------------------------------------------------------- #
N_phases	2

# Set initial cell dimensions and choose init file to load
CellDim    {} {} {}
initial_ms	  {}

# Type of phase-1 and phase-2 (in accordance with the following order)
# 0(xtal) 1(gas) ie, the 1st phase is PhaseID 0 and the 2nd phase is PhaseID 1
Type_phases		0	1

# Elastic constants of phase 1: c11, c12, c44, nu
# Monocrystalline SS_304 from Ledbetter_1984 (see References folder)
#ElastConst_PhaseI	204600. 137700 126200 0.334
ElastConst_PhaseI	261400. 106600 77400 0.3

# Data file containing info of phase 1
Slip_PhaseI		SS_304.sx

# Elastic constants of phase 1: c11, c12, c44, nu
# Gas, ignored!
ElastConst_PhaseII	0.0	0.0	0.0	0.0

# Data file containing info of phase 2
Slip_PhaseII	SS_304.sx


#====================================================
#     Boundary conditions
#----------------------------------------------------ss
# Flags for velocity gradient (0:unknown, 1:known) Voigt notation
VelGrad_BC_Flag         0 0 1 1 1 1

# Velocity gradient
VelGrad_BC              -0.5E-3 -0.5E-3 1.0E-3 0.0 0.0 0.0

# Flags for Cauchy stress
Stress_BC_Flag          1 1 0 0 0 0

# Applied Cauchy stress
Stress_BC       0. 0. 0. 0. 0. 0.

# elasticity BC: 0 -(e_homo=0), 1 -(relaxed bc)
ElastBC         1

#====================================================
#	Simulation setup
#----------------------------------------------------
# Time increment
TimeStep 0.5

# Total simulation steps
N_steps		1

# Error tolerance
Err		5E-5

# Maximum iteration steps allowed to solve micromechanical problem
IterMax		2000

# write fields?(0:No, 1:Yes)	write step interval
PrintControl	1     1

# Update texture flag
Update_Flag		1

# Hardening flag
Hard_Flag		1

# Texture output flag
Tex_Flag		1


#====================================================
#	Dislocation density based constitutive model
#----------------------------------------------------
# A. Ma et al. Acta Mater. 54(2006) 2169
# lattice parameters for each phase [nm]
a0	0.362	0.0
# voxel length used in GND [mm]
#L0	0.03
L0     0.003
# Used to scale L0 to saturate GND
#GND_ScaleRatio	1000.0
GND_ScaleRatio	6000.0
# Activation energy for slip [1E-19J]
Q_slip		20000000000.0	0.0
# Activation energy for climb [1E-19J]
Q_bulk		300000000000.51 0.0
# Initial SSD density [1E12/m^2]
rho_SSD_initial	   0.05
# Temperature [K]
T__K  30
# Constant for passing stress
C_1		0.48	0.0
# Constant for jump width
C_2		3.0	0.0
# Constant for obstacle width
C_3		3.0	0.0
# Constant for lock forming rate [1E6/m]
C_4 	  800.0 	0.0
## Constant for 6thermal annihilation rate 
C_5	16.0	0.0
## Constant for thermal annihilation rate [1/nm]
C_6	1100E0	0.0

## Constant for dipole forming rate [1E-37m^5*s^C_8/s]
C_7		1.25E-8	0.0
#C_7		0.0	0.0
# Constant for nonlinear climb of edge dislocations
C_8		0.24	0.0
# Self interaction
Selfinter	0.125	0.0
# coplanar interaction
Coplanar	0.125	0.0
# cross slip
CrossSlip	0.725	0.0
#CrossSlip	0.8	0.0
# glissile junction
GlissileJunction	0.125 0.0
#GlissileJunction	0.8 0.0
# Hirth loc
HirthLock	0.05	0.0
#HirthLock	0.8	0.0
# Lomer-Cottrell Lock
LomerCottrellLock 0.18 0.0
#LomerCottrellLock	0.8 0.0
#Diffusion coefficient for climb equation
D_0     4E-05
#nucleus_radius
nucleus_radius  4
#====================================================
#	Phase field of recrystallization
#----------------------------------------------------
# System size for Phase-field (not need to be equal to FFDynamic recrystallization of copper polycrystals with different puritiesT size)
CellDim_pf	64 64 64
#CellDim_pf 512 512 1
# Seed f32 GSL random number generator
RandSeed 123
# GB energy [J/m^2]
E_gb	0.625
# GB mob2lity [m^4/(MJ*s)]
M_gb  7.4E-11
# Burgers vector [nm]
bb_len		0.256
# Scaler of stored energy [1]
Scale_Fdeform	0.5
# Characteristic nucleation strength (disl. density difference) for DRX [1E12/m^2]
k_c 320000.0
# Expon0nt in the statistical model for DRX nucleation [1]
alpha 8.5
# Norm3lized GB mobility in phase-field [in grid unit]
# inversely proportional to the # of phase-field steps
#M_bar   12.0
# Coefficient to be multiplied to k_c for re-nucleation
zeta_kc 1.0
# Aging time for DRX re-nucleation
age_drx 1E0
# Scaler used to be multiplied to SSDs for DRX grains after pahse-field
ssdScaler_DRX 0.74
gndScaler_DRX  0.66
sigScaler_DRX	0.66
# Static or dynamic implementation of nucleation? (1: static, 0: dynamic)
Nucl_Static_Flag	1
# Characteristic aging time for newly-formed DRX grains, normalized by TimeStep
tau_DRX	1000.0
# Additional barrier for slip for new DRX grains [J/m^2]
DeltaQ_DRX	0.0
# Additional passing stress for new DRX grains 
DeltaTau_DRX	0.0
#pf_length_scale
pf_length_scale     2.5E-06
#pf_time_step
pf_time_step        0.004

""".format(date,x,y,z,init_name))


def SX_text_gen():
    return("""# Crystal system (XtalSys):
CUBIC

# Crystal axex (XtalAxis):
1.0 1.0 1.0

# Total number of modes listed in the file (N_modes_max):
3

# Number of modes to be used in the calculation (N_modes):
1

# Labels of the modes to be used (iMode):
1

# Mode# 7 -- <110>(111) SLIP (same as Mode#1, used to check PlasticInit())
	nsmx	nrsx	gamd0x	twshx	isectwx
	6		10.		1.0		0.0		0
	tau0xf	tau0xb	tau1x	thet0	thet1
	150.0		150.0		5.		400.	250.
	hselfx	hlatex
	1.0		1.0
	Slip (n-b)
	1 1 -1		0 1 1
	1 1 -1		1 0 1
	1 1 -1		1 -1 0
	1 -1 -1		0 1 -1
	1 -1 -1		1 0 1
	1 -1 -1		1 1 0
# Mode# 1 -- <110>(111) SLIP
	nsmx	nrsx	gamd0x	twshx	isectwx
	12		10.		1.0		0.0		0
	tau0xf	tau0xb	tau1x	thet0	thet1
	150.0		150.0		5.		400.	250.
	hselfx	hlatex
	1.0		1.0
	Slip (n-b)
	1 1 -1		0 1 1
	1 1 -1		1 0 1
	1 1 -1		1 -1 0
	1 -1 -1		0 1 -1
	1 -1 -1		1 0 1
	1 -1 -1		1 1 0
	1 -1 1		0 1 1
	1 -1 1		1 0 -1
	1 -1 1		1 1 0
	1 1 1		0 1 -1
	1 1 1		1 0 -1
	1 1 1		1 -1 0
# Mode# 100 <110>(111) SLIP
	nsmx	nrsx	gamd0x	twshx	isectwx
	12		10.		1.0		0.0		0
	tau0xf	tau0xb	tau1x	thet0	thet1
	9.0		9.0		5.		400.	250.
	hselfx	hlatex
	1.0		1.0
	Slip (n-b)
	1 1 -1		0 1 1
	1 1 -1		1 0 1
	1 1 -1		1 -1 0
	1 -1 -1		0 1 -1
	1 -1 -1		1 0 1
	1 -1 -1		1 1 0
	1 -1 1		0 1 1
	1 -1 1		1 0 -1
	1 -1 1		1 1 0
	1 1 1		0 1 -1
	1 1 1		1 0 -1
	1 1 1		1 -1 0
""")


def Create_Simulation(folder_name,init,init_name,xpad,ypad,zpad):
    folder_location ="Data/02_forge_input/"+folder_name 
    xlen,ylen,zlen = init.shape
    
    #Initial assertion checks to prevent overwriting data
    if len(glob.glob(folder_location)) == 0:
        os.mkdir(folder_location)
    else:
        errortxt = """
ERROR: The chosen folder has completed results in it. This script will not
write to a folder with completed results. To write out folder, either change
the foldername for this function, or move the existing data to a different
location"""
        assert len(glob.glob(folder_location+"/*.iout")) == 0, errortxt
        assert len(glob.glob(folder_location+"/*.iout*")) == 0, errortxt
        assert len(glob.glob(folder_location+"/*.out")) == 0, errortxt
    
    #Write Text files
    with open(folder_location+"/input.in",'w', newline ='') as infile:
        text = input_text_gen(zlen+zpad, ylen+ypad+2, xlen+xpad,init_name)
        infile.write(text)
        infile.close()
    
    with open(folder_location+"/PBS_instructions.txt",'w', newline ='') as infile:
        infile.write("""Code for interactive Batch:
sinteractive -N 1 -n 48 -p largemem -t 02:00:00 -A PAA0023 bigmem_Forge_{}
Alternate for if the largemem nodes are taken/slow:
sinteractive -N 2 -n 48 -p parallel -t 8:00:00 -A PAA0023 large_boi_{}

Command for running forge (works for Austin anyway, may not for you):
mpiexec ~/bin/forge/input.in
""".format(folder_name,folder_name))
        infile.close()        
    
    with open(folder_location+"/SS_304.sx",'w', newline ='') as infile:
        text = SX_text_gen()
        infile.write(text)
        infile.close()
    
    
    #Create initial Simulation
    forge_init = np.zeros([xpad+xlen,ypad+ylen+2,zpad+zlen],dtype=np.uint8)+255
    forge_init[xpad//2:xlen+xpad-(xpad//2),
               1+ypad:-1,
               zpad//2:zlen+zpad-(zpad//2):]=init
    forge_init[:,0,:] = 200
    forge_init[:,-1,:] = 200
    
    
    # make an stl
    to_stl =np.zeros([xlen+xpad+2,
                      ylen+ypad+2,
                      zlen+zpad+2])
    to_stl[1:-1,:,1:-1]=forge_init
    verts,faces =measure.marching_cubes(to_stl, level = 254)[:2]
    del to_stl
    #Make a container for holding the stl data and populate it
    stl_data = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))
    for i, f in enumerate(faces):
        for j in range(3):
            stl_data.vectors[i][j] = verts[f[j], :]
    # Save as STL file
    stl_data.save(folder_location+"/SS304.stl")
    del faces,verts,stl_data
    
        
    # create input file
    xrange,yrange,zrange = forge_init.shape
    n = xrange*yrange*zrange
    
    #Stupid math to turn Bulk and air into correct phases for FORGE
    mask = forge_init == 255
    mask = mask.reshape(n)+1
    air = 3-mask
    
    x = (np.arange(n)%xrange)+1
    y = (np.arange(n)//xrange%yrange)+1
    z = (np.arange(n)//yrange//xrange)+1
    
    dat = np.vstack([x*0,x*0,x*0,z,y,x,mask,air]).T
    init_savename = folder_location+"/"+init_name
    np.savetxt(init_savename,dat,
               fmt='%0.1f %0.1f %0.1f %i %i %i %i %i',
               delimiter= ' ', newline ='\n')
    return()







def strip_metadata_from_input(input_file_location):
    with open(input_file_location, "r") as infile:
        text = infile.read()
        infile.close()
    # make a copy of the text sans comments for easy parsing
    txt = "\n".join([x.split("#")[0] for x in text.split("\n")])
    # find the celldim line and extract the values from it (this is a really dumb way
    # to do this btw, but it works. google Regex for the 'right' method)
    x, y, z = np.array(
        re.sub(" +", " ", txt.split("CellDim")[1].split("\n")[0]).split(" ")[1:]
    ).astype(np.int)
    n = x * y * z
    # add more data grabbing here if desired

    # now make dictionary
    Meta_Dictionary = {"x": x, "y": x, "z": x, "xyz": np.array([x, y, z]), "n": n}
    return Meta_Dictionary


def get_xyz(MD_dict, dimensions, location):
    if type(MD_dict) == dict:
        try:
            dimensions = MD_dict["xyz"]
        except:
            print(
                """Import from provided dict failed, skipping to next
method for determining dimensions"""
            )
    if dimensions == "infer":
        input_file = glob.glob(location + "/*.in")
        assert (
            len(input_file) == 1
        ), """
error: more than one input file in folder. Either remove duplicate input files,
or explicitly pass in xyz dimensions"""
        MD_dict = strip_metadata_from_input(input_file[0])
        dimensions = MD_dict["xyz"]
    x, y, z = dimensions
    return (x, y, z)


def sig6_binary_to_array(location, prefix="sig_", dimensions="infer", MD_dict="N/A"):
    assert os.path.isdir(location), " Chosen Folder path does not exist"
    filelist = np.array(glob.glob("{}/{}*.iout*".format(location, prefix)))
    assert (
        len(filelist) == 6
    ), """
Whoops, looks like you dont have exactly 6 stress file binaries! Double check
to make sure you have correctly downloaded all files, that your location variable
is correct, that that you passed in a binary prefix unique to the sigma files
in question."""
    filelist.sort()
    # Now a little helper function for getting dimensions of simulation.
    x, y, z = get_xyz(MD_dict, dimensions, location)
    # this is the actual sigma file reader. reads binaries into 6 numpy arrrays
    sig = [np.frombuffer(open(file, "rb").read(), np.double) for file in filelist]
    # This reshapes sig according to x,y,z
    sigmas = [s.reshape(z, y, x) for s in sig]
    return sigmas


def sig6_binary_to_array_zxy(location, prefix="sig_", dimensions="infer", MD_dict="N/A"):
    assert os.path.isdir(location), " Chosen Folder path does not exist"
    filelist = np.array(glob.glob("{}/{}*.iout*".format(location, prefix)))
    assert (
        len(filelist) == 6
    ), """
Whoops, looks like you dont have exactly 6 stress file binaries! Double check
to make sure you have correctly downloaded all files, that your location variable
is correct, that that you passed in a binary prefix unique to the sigma files
in question."""
    filelist.sort()
    # Now a little helper function for getting dimensions of simulation.
    x, y, z = get_xyz(MD_dict, dimensions, location)
    # this is the actual sigma file reader. reads binaries into 6 numpy arrrays
    sig = [np.frombuffer(open(file, "rb").read(), np.double) for file in filelist]
    # This reshapes sig according to x,y,z
    sigmas = [s.reshape(z, x, y) for s in sig]
    return sigmas
def sig6_binary_to_array_xzy(location, prefix="sig_", dimensions="infer", MD_dict="N/A"):
    assert os.path.isdir(location), " Chosen Folder path does not exist"
    filelist = np.array(glob.glob("{}/{}*.iout*".format(location, prefix)))
    assert (
        len(filelist) == 6
    ), """
Whoops, looks like you dont have exactly 6 stress file binaries! Double check
to make sure you have correctly downloaded all files, that your location variable
is correct, that that you passed in a binary prefix unique to the sigma files
in question."""
    filelist.sort()
    # Now a little helper function for getting dimensions of simulation.
    x, y, z = get_xyz(MD_dict, dimensions, location)
    # this is the actual sigma file reader. reads binaries into 6 numpy arrrays
    sig = [np.frombuffer(open(file, "rb").read(), np.double) for file in filelist]
    # This reshapes sig according to x,y,z
    sigmas = [s.reshape(x, z, y) for s in sig]
    return sigmas
def sig6_binary_to_array_zyx(location, prefix="sig_", dimensions="infer", MD_dict="N/A"):
    assert os.path.isdir(location), " Chosen Folder path does not exist"
    filelist = np.array(glob.glob("{}/{}*.iout*".format(location, prefix)))
    assert (
        len(filelist) == 6
    ), """
Whoops, looks like you dont have exactly 6 stress file binaries! Double check
to make sure you have correctly downloaded all files, that your location variable
is correct, that that you passed in a binary prefix unique to the sigma files
in question."""
    filelist.sort()
    # Now a little helper function for getting dimensions of simulation.
    x, y, z = get_xyz(MD_dict, dimensions, location)
    # this is the actual sigma file reader. reads binaries into 6 numpy arrrays
    sig = [np.frombuffer(open(file, "rb").read(), np.double) for file in filelist]
    # This reshapes sig according to x,y,z
    sigmas = [s.reshape(z, y,x) for s in sig]
    return sigmas




def els_binary_to_array(location, prefix="els_", dimensions="infer", MD_dict="N/A"):
    assert os.path.isdir(location), " Chosen Folder path does not exist"
    filelist = np.array(glob.glob("{}/{}*.iout*".format(location, prefix)))
    assert (
        len(filelist) == 6
    ), """
Whoops, looks like you dont have exactly 6 els binaries! Double check
to make sure you have correctly downloaded all files, that your location variable
is correct, that that you passed in a binary prefix unique to the els files
in question."""
    filelist.sort()
    # Now a little helper function for getting dimensions of simulation.
    x, y, z = get_xyz(MD_dict, dimensions, location)
    # this is the actual els file reader. reads binaries into 6 numpy arrrays
    els = [np.frombuffer(open(file, "rb").read(), np.double) for file in filelist]
    # This reshapes sig according to x,y,z
    elss = [s.reshape(z, y,x) for s in els]
    return elss


def gID_binary_to_array(
    location, prefix="Aus_Grain", dimensions="infer", MD_dict="N/A"
):
    assert os.path.isdir(location), " Chosen Folder path does not exist"
    filelist = np.array(glob.glob("{}/{}*.iout*".format(location, prefix)))
    assert (
        len(filelist) == 1
    ), """
Whoops, there should only be a single gID file, but you have chosen {}""".format(
        len(filelist)
    )
    # Now a little helper function for getting dimensions of simulation.
    x, y, z = get_xyz(MD_dict, dimensions, location)
    # this is the actual els file reader. reads binaries into 6 numpy arrrays
    gID = np.frombuffer(open(filelist[0], "rb").read(), np.int).reshape(z, y, x)
    return gID



def orientation_tex_to_array(
    location, prefix="tex_", dimensions="infer", MD_dict="N/A"
):
    assert os.path.isdir(location), " Chosen Folder path does not exist"
    filelist = np.array(glob.glob("{}/{}*.iout*".format(location, prefix)))
    assert (
        len(filelist) == 1
    )
    file = filelist[0]
    # Now a little helper function for getting dimensions of simulation.
    x, y, z = get_xyz(MD_dict, dimensions, location)
    # this is the actual els file reader. reads binaries into 6 numpy arrrays
    tex = np.frombuffer(open(file, "rb").read(), np.double)
    # This reshapes sig according to x,y,z
    return tex

