# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 04:38:57 2021

@author: arger

This script has 3 steps:
    1) Read in original CT image slices
    2) Save data as more efficient HDF file (01_Initial/Initial_Structure.h5)
    3) Create STL surface mesh files of the part for easier viewing

=============================================================================
    NOTE: This never needs to be ran unless using new CT data. Code is only 
    here for clarity on the methodology.
=============================================================================
"""

import numpy as np
from skimage import measure
from stl import mesh
import functions as fn
from zipfile import ZipFile
import glob,os

## DONT UNCOMMENT THIS UNLESS YOU USE NEW TIF FILES.!!!
## It takes any TIF files in the directory and add to the zip folder
# os.chdir("Data/00_Slices")
# zf =ZipFile( "TIFs2.zip",'a')
# for file in glob.glob("*.tif"):
#     zf.write(file)
# zf.close()
# os.chdir("../../")


print("""
Initial consolidation is starting.

============================================================================
NOTE: there is no need to run this file if the starting data has not changed
This is only here for transparency of how the starting data was computed
============================================================================

""")
print("Unzipping TIF folder...\n\n")

# quick little unzip operation
zf =ZipFile( "Data/00_slices/TIFs.zip",'r')
zf.extractall("Data/00_slices")
zf.close()


print("TIF files unzipped \n\n consilidating cuts and saving to hdf...\n\n""")

# Run a function from "functions.py" to convert tifs to HDF
data_stack = fn.HDF5_from_slices(
    "Data/00_slices", "Data/01_Initial/01_Initial_Structure"
)

# Also grab the Tiff-to-Pore_ID dictionary (helps decode info in TIF files)
init_dict = fn.get_init_dict()

print("hdf Created \n\n delete the copied TIF files...")
for f in glob.glob("Data/00_slices/*.tif"):
    os.remove(f)
print("hdf Created \n\n Building STL surface meshes...")

# This next group takes awhile to run and is a memory hog, can crash a computer
# with less than 16 GM of RAM. It uses something called marching cubes technique
# to turn a 3D array of raster data (which takes up several GB's)
# into the data needed to generate an STL file of an isosurface (WAY smaller).
vert_and_face_list = []
vert_and_face_list.append(measure.marching_cubes(data_stack, level=179))
data_stack[data_stack > 179] = 0
print("Bulk  done (level 1 of 9)...")
vert_and_face_list.append(measure.marching_cubes(data_stack, level=159))
data_stack[data_stack > 159] = 0
print("Pore 8 done (level 2 of 9)...")
vert_and_face_list.append(measure.marching_cubes(data_stack, level=139))
data_stack[data_stack > 139] = 0
print("Pore 7 done (level 3 of 9)...")
vert_and_face_list.append(measure.marching_cubes(data_stack, level=119))
data_stack[data_stack > 119] = 0
print("Pore 6 done (level 4 of 9)...")
vert_and_face_list.append(measure.marching_cubes(data_stack, level=99))
data_stack[data_stack > 99] = 0
print("Pore 5 done (level 5 of 9)...")
vert_and_face_list.append(measure.marching_cubes(data_stack, level=79))
data_stack[data_stack > 79] = 0
print("Pore 4 done (level 6 of 9)...")
vert_and_face_list.append(measure.marching_cubes(data_stack, level=59))
data_stack[data_stack > 59] = 0
print("Pore 3 done (level 7 of 9)...")
vert_and_face_list.append(measure.marching_cubes(data_stack, level=39))
data_stack[data_stack > 39] = 0
print("Pore 2 done (level 8 of 9)...")
vert_and_face_list.append(measure.marching_cubes(data_stack, level=19))
print("Pore 1 done (level 9 of 9)...")


# Make a container for holding the stl data and populate it
print("\n\n Building surface meshes from the data...\n\n")
stl_container = []
iii = 1
for vfl in vert_and_face_list:
    print("Packaging stl map {} of 9".format(iii))
    iii +=1
    stl_data = mesh.Mesh(np.zeros(vfl[1].shape[0], dtype=mesh.Mesh.dtype))
    for i, f in enumerate(vfl[1]):
        for j in range(3):
            stl_data.vectors[i][j] = vfl[0][f[j], :]
    stl_container.append(stl_data)


# Finally, save out the layers as actual STL files.
print("STL surface meshes finished. \n\n Saving as STL files...\n")

save_loc = "Data/01_Initial"
for i in np.arange(9):
    filename = "{}/{}.stl".format(save_loc, init_dict[180 - (i * 20)])
    stl_container[i].save(filename)
    print("{} saved!".format(filename))

print("Done")
