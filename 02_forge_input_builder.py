# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 05:02:07 2021

@author: arger


===============================================================================
In the last script (01_Tiff_to_HDF.py), an HDF file was make which holds
the entire CT scan. However, we only want to look at small sections at a time.
This program does the following:
    1) section out a particular section of interest
    2) reorient the part so your pore and surface are orthogonal to the axes
    3) writes out a simulation folder and all necessary simulation files
===============================================================================

"""

import numpy as np
import glob
import h5py
import matplotlib.pyplot as plt
import functions as fn

# Step 1: load up the inital HDF file in read-only mode.
print("loading Initial Microstructure \n")
initial = fn.Load_Initial_Microstructure("Data/01_Initial/01_Initial_Structure.h5")
# Also grab the Tiff-to-Pore_ID dictionary, as well as an inverted version
init_dict = fn.get_init_dict()
inv_init_dict = {v: k for k, v in fn.get_init_dict().items()}

print("Initial Microstructure loaded")


# Step 2: Pick out some simulation regions, align them, and return the aligned volume
# There are a BUNCH of ways to do this depending on what you want to simulate.
# Below is an example of how to slice up Pore 1. To find the xyz values, I just
# opened up the stl file in Paraview and made slices to find the right range,
# Then started playing with the plotting values here until I got values I liked
print("getting initial Pore_1 array...")
Pore_1_data = fn.Cut_Out_Pore(
    x_start=1080,
    y_start=145,
    z_start=1105,
    xlen=100,
    ylen=43,
    zlen=90,
    initial=initial,
    Pore_ID=1,
    ypad = 10,
    xrot=0.01,
    zrot=-0.04
    )[0]
print("Done\n")
# However, this is the slow way to do it which uses all the extra plotting
# functions. Below is how to do Pore 8,4 and 5 the fast way, when youve already
# done the guess-and-check and you know for sure the correct values for the
# start, range, and tilt

print("getting initial Pore_8 array...")
Pore_8_data = fn.Cut_Out_Pore(190, 250, 670, 120, 26, 200, initial,8,'f','f','f')[0]
print("Done\n")

# Sometimes, based on the local surface, it makes sense to tilt the pore to
# better align with the orthogonal directions, such as done here for Pore 4
print("getting initial Pore_4 array...")
Pore_4_data = fn.Cut_Out_Pore(550, 200, 800, 130, 41, 180, initial, 4,'f','f','f',zrot=-0.005)[0]
print("Done\n")

print("getting initial Pore_5 array...")
Pore_5_data = fn.Cut_Out_Pore(560, 200, 980, 50, 36, 70, initial, 2, "f", "f", "f", zrot=-0.01)[0]
print("Done\n")



# Step 3: Smooth surface and fill air space
"""
=============================================================================
NOTE: This Step creates some fake data! It takes the part of the simulation
at the surface (at most about 2-3 layers) which currently has a rough 
surface and intelligently fills the space to match what is under it. 
IF YOU DON'T WANT THIS STEP, skip this section and shave a few extra slices 
from the ylen variable in the step above. DO NOTE THOUGH, this will cause you
to lose some data about the pore shape close to the surface. BOTH METHODS 
HAVE VALID PROS AND CONS
=============================================================================
"""
print("Smoothing Pore 1")
Pore_1_init = fn.Surface_Cleaner(Pore_1_data,inv_init_dict['Pore_1'])
print("Done\n\n")

print("Smoothing Pore 4")
Pore_4_init = fn.Surface_Cleaner(Pore_4_data,inv_init_dict['Pore_4'])
print("Done\n\n")

print("Smoothing Pore 5")
Pore_5_init = fn.Surface_Cleaner(Pore_5_data,inv_init_dict['Pore_5'])
print("Done\n\n")

print("Smoothing Pore 8")
Pore_9_init = fn.Surface_Cleaner(Pore_8_data,inv_init_dict['Pore_8'])
print("Done\n\n")

# Step 4: Make input for forge to process on the OSC


"""
=============================================================================
NOTE: This is the part where you can just start making whatever simulations you
want. Add padding, remove it, change input/output locations, make copies of 
experiments in order to edit details by hand, whatever.
=============================================================================
"""

print("Creating 3 different sized simulations for Pore 1...")
# NOTE: this function is where the extra padding is added. Format is as follows:
#fn.Create_Simulation('foldername', init_array,'Init_filename',xpad,ypad,zpad)
fn.Create_Simulation('Pore_1_deep',Pore_1_init,"Pore_1.init",150,69,120)
fn.Create_Simulation('Pore_1_tiny',Pore_1_init,"Pore_1_tiny.init",8,6,8)
#fn.Create_Simulation('Pore_1',Pore_1_init,"Pore_1.init",150,19,120)
#fn.Create_Simulation('Pore_1_large',Pore_1_init,"Pore_1.init",200,49,170)
print("DONE!")