# Pit_Modeling
by Austin Gerlt (gerlt.1@osu.edu)

This is the code for taking the CT scans, converting them into data the forge
can read, and then taking those values and converting them back into something
a human can understand. This is all pure python

Basic outline of python scripts:

01_Tiff_to_HDF.py            -- consolidate to hdf (Dont run this unless you 
                                have new TIFs)
02_forge_input_builder.py    -- setup for OSC forge operation
03_forge_to_HDF.py           --decode forge output (currently not implemented)


REQUIRED PYTHON LIBRARIES THAT ARENT IN STANDARD ANACONDA:
(written in the form of the pip commands you need):

pip install numpy-stl
pip install zipfile
pip install transformations

OTHER NOTES:
The .git folder for this repo is pretty bloated due to the way I did
LFS, will try to shrink it down some over time

Need to fix this README as well, very bad, much improvement. 

